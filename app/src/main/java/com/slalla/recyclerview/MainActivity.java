package com.slalla.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    String s1[], s2[];
    int images[] = {R.drawable.ic_baseline_accessible_forward_24, R.drawable.ic_baseline_accessible_forward_24,
            R.drawable.ic_baseline_accessible_forward_24, R.drawable.ic_baseline_accessible_forward_24,
            R.drawable.ic_baseline_accessible_forward_24, R.drawable.ic_baseline_accessible_forward_24,
            R.drawable.ic_baseline_accessible_forward_24, R.drawable.ic_baseline_accessible_forward_24,
            R.drawable.ic_baseline_accessible_forward_24};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        s1 = getResources().getStringArray(R.array.programming_languages);
        s2 = getResources().getStringArray(R.array.programming_descriptions);
        recyclerView = findViewById(R.id.recycle);

        MyAdapter myAdapter = new MyAdapter(this, s1, s2, images);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}